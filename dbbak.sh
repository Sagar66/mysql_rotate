#!/bin/bash
#================================================================
# HEADER
#================================================================
#% SYNOPSIS
#+    ${SCRIPT_NAME}
#%
#% DESCRIPTION
#%    This is a script to backup and rotate mysql database.
#%    Please execute it as root..
#%
#% OPTIONS
#% Just execute for now.
#%	!!!-OPEN FOR ANY ADVISE-!!!
#% 
#% EXAMPLES
#%  N/A
#%
#================================================================
#- IMPLEMENTATION
#-    version         ${SCRIPT_NAME} 0.0.0.1 (alpha)
#-    author          Sagar Khatiwada(sagar.khatiwada66@gmail.com)
#-    copyright       N/A
#-    license         N/A
#-    script_id/      0003
#-    PushCount		
#================================================================
#  HISTORY
#     2020/04/11 : sks63 : Script creation
# 
#================================================================
#  DEBUG OPTION
#    set -n  # Uncomment to check your syntax, without execution.
#    set -x  # Uncomment to debug this shell script
#    set -v  # Uncomment to debug on verbose mode
#================================================================
#  BUGS
#
#================================================================
# END_OF_HEADER
#================================================================
# BEGAIN
#================================================================
# Exit on error/un used variable check/pipe fail check
set -o errexit 
set -o nounset
set -o pipefail
#================================================================
# VARIABLES
#Define path variable for cron.
PATH=""
#DB username
USER="bankex"
#DB password
PASSWORD="bankex"
#backup dir
OUTPUTDIR="./DB_DUMPS"
#rotate time in days.
DELETE_AFTER="1"
#databases to backup.
DATABASES=(EXPRESSBANK IPS)
#================================================================
# STATIC
ABSOLUTE_PATH=$( cd "$(dirname "$0")" ; pwd -P )
MASTER_LOG="$ABSOLUTE_PATH"/Master.log
mkdir -p "$ABSOLUTE_PATH"/DB_DUMPS
#================================================================
# MAIN
echo "################################ Action taken at $(date +%Y-%m-%d@%H:%M:%S) ###############################################" >> ./Master.log
if [ ! -d "$OUTPUTDIR" ]; then
	mkdir -p "$OUTPUTDIR";
fi
for db in "${DATABASES[@]}"
do
	mysqldump --force --single-transaction --opt -u "$USER" -p"$PASSWORD" --databases "$db" | gzip > ""$OUTPUTDIR"/"$db"/"$db"_$(date +%Y-%m-%d@%H:%M:%S).sql.gz"
	FILES=$(cd "$OUTPUTDIR"/"$db" && find . -maxdepth 1 -type f -name "${db}*" -mtime +$DELETE_AFTER | tail -1) 2>> ./Master.log
	removeFile(){
		echo ""$db" Backup completed" >> ./Master.log
		if [ "$FILES" == "" ];then
			echo "No files to delete" >> ./Master.log 2>&1
		else
			echo "These are the files to be deleted:- $FILES" >> ./Master.log 
			cd "$OUTPUTDIR"/"$db" && rm -fv $FILES >> $MASTER_LOG 2>&1
			cd "$ABSOLUTE_PATH"
		fi
	}
	removeFile
done
echo "################################ Done Done Done ###############################################" >> ./Master.log

#======================== (END) =================================
