#!/bin/bash
# Exit on error/un used variable check/pipe fail check
set -o errexit
set -o nounset
set -o pipefail
#================================================================
# VARIABLES
#Define path variable for cron.
PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/root/bin"
#Login path name
LOGIN="dbBak"
#DB username
USER="DBBAK"
#DB password
PASSWORD=""
#backup dir
OUTPUTDIR="DB_DUMPS"
#rotate time in days.
DELETE_AFTER="90"
#databases to backup.
DATABASES=(BANK IPS REPORT)
#================================================================
# STATIC
#ABSOLUTE_PATH=$( cd "$(dirname "$0")" ; pwd -P )
ABSOLUTE_PATH=$( dirname "$(realpath $0)")
echo $ABSOLUTE_PATH
MASTER_LOG="$ABSOLUTE_PATH"/Master.log
#mkdir -p "$ABSOLUTE_PATH"/DB_DUMPS
#================================================================
# MAIN
echo "###################### Action taken at $(date +%Y-%m-%d@%H:%M:%S) ####################################" >> ./Master.log
#if [ ! -d "$OUTPUTDIR" ]; then
#       mkdir -p "$OUTPUTDIR";
#fi
for db in "${DATABASES[@]}"
do
        mkdir -p "$ABSOLUTE_PATH"/"$OUTPUTDIR"/"$db"
##Method MySql Path
        mysqldump --login-path="${LOGIN}" --force --single-transaction --no-tablespaces --databases "${db}" | gzip > ""$OUTPUTDIR"/"$db"/"$db"_$(date +%Y-%m-%d@%H:%M:%S).sql.gz"

#Method Normal Password
        #mysqldump --force --single-transaction --opt -u "$USER" -p"$PASSWORD" --databases "$db" | gzip > ""$OUTPUTDIR"/"$db"/"$db"_$(date +%Y-%m-%d@%H:%M:%S).sql.gz"
        FILES=$(cd "$OUTPUTDIR"/"$db" && find . -maxdepth 1 -type f -name "${db}*" -mtime +$DELETE_AFTER | tail -1) 2>> ./Master.log
        removeFile(){
                echo ""$db" Backup completed" >> ./Master.log
                if [ "$FILES" == "" ];then
                        echo "No files to delete" >> ./Master.log 2>&1
                else
                        echo "These are the files to be deleted:- $FILES" >> ./Master.log
                        cd "$OUTPUTDIR"/"$db" && rm -fv $FILES >> $MASTER_LOG 2>&1
                        cd "$ABSOLUTE_PATH"
                fi
        }
        removeFile
done
echo "################################ Done Done Done ###############################################" >> ./Master.log
echo Completed at $(date)
#======================== (END) =================================
